package com.example.product.service.impl;

import com.example.product.entities.ProductEntity;
import com.example.product.exception.EntityNotFoundException;
import com.example.product.mapper.ProductMapper;
import com.example.product.model.Product;
import com.example.product.repository.ProductRepository;
import com.example.product.service.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class ProductServiceImpl implements ProductService {

    @Autowired
    ProductRepository productRepository;

    @Autowired
    ProductMapper productMapper;

    public Product getById(Long id) {
        Optional<ProductEntity> productEntity = productRepository.findById(id);
        if (productEntity.isPresent()) {
            return productMapper.entityToModel(productEntity.get());
        }
        throw new EntityNotFoundException(EntityNotFoundException.NOT_FOUND_UNDER_ID);
    }

    public Product create(Product product) {
        ProductEntity productEntity = productMapper.modelToEntity(product);
        productEntity = productRepository.save(productEntity);

        product.setId(productEntity.getId());
        return product;
    }

    public Product update(Product product) {
        Optional<ProductEntity> productEntityOptional = productRepository.findById(product.getId());
        if (!productEntityOptional.isPresent()) {
            throw new EntityNotFoundException(EntityNotFoundException.NOT_FOUND_UNDER_ID);
        }
        ProductEntity productEntity = productMapper.modelToEntity(product);
        productRepository.save(productEntity);

        return product;
    }

    public void deleteById(Long id) {
        productRepository.deleteById(id);
    }

    public void increaseTotal(Long id, Long num) {
        Optional<ProductEntity> productEntityOptional = productRepository.findById(id);
        if (!productEntityOptional.isPresent()) {
            throw new EntityNotFoundException(EntityNotFoundException.NOT_FOUND_UNDER_ID);
        }
        Long total = productEntityOptional.get()
                .getTotal() + num;

        productRepository.updateTotalById(id, total);
    }

    public void reduceTotal(Long id, Long num) {
        Optional<ProductEntity> productEntityOptional = productRepository.findById(id);
        if (!productEntityOptional.isPresent()) {
            throw new EntityNotFoundException(EntityNotFoundException.NOT_FOUND_UNDER_ID);
        }
        Long total = productEntityOptional.get()
                .getTotal() - num;

        productRepository.updateTotalById(id, total);
    }

    public void increaseReserved(Long id, Long num) {
        Optional<ProductEntity> productEntityOptional = productRepository.findById(id);
        if (!productEntityOptional.isPresent()) {
            throw new EntityNotFoundException(EntityNotFoundException.NOT_FOUND_UNDER_ID);
        }
        Long reserved = productEntityOptional.get()
                .getReserved() + num;

        productRepository.updateReservedById(id, reserved);
    }

    public void reduceReserved(Long id, Long num) {
        Optional<ProductEntity> productEntityOptional = productRepository.findById(id);
        if (!productEntityOptional.isPresent()) {
            throw new EntityNotFoundException(EntityNotFoundException.NOT_FOUND_UNDER_ID);
        }
        Long reserved = productEntityOptional.get()
                .getReserved() - num;

        productRepository.updateReservedById(id, reserved);
    }
}
