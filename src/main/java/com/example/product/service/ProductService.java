package com.example.product.service;

import com.example.product.model.Product;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Optional;

public interface ProductService {

    Product getById(Long id);

    Product create(Product product);

    Product update(Product product);

    void deleteById(Long id);

    void increaseTotal(Long id, Long num);

    void reduceTotal(Long id, Long num);

    void increaseReserved(Long id, Long num);

    void reduceReserved(Long id, Long num);
}
