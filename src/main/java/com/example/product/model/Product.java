package com.example.product.model;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.FieldDefaults;

import java.math.BigDecimal;

@FieldDefaults(level = AccessLevel.PRIVATE)
@Getter
@Setter
public class Product {

    Long id;

    Type type;

    String name;

    String description;

    BigDecimal price;

    Long total;

    Long reserved;
}
