package com.example.product.exception;

public class EntityNotFoundException extends RuntimeException {

    public static final String NOT_FOUND_UNDER_ID = "Can not find any product under given ID";

    public EntityNotFoundException(String message) {
        super(message);
    }

    public EntityNotFoundException(String message, Throwable cause) {
        super(message, cause);
    }
}
