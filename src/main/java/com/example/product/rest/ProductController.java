package com.example.product.rest;

import com.example.product.model.Product;
import com.example.product.service.ProductService;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@AllArgsConstructor
@RestController
@RequestMapping(value = "/product", produces = MediaType.APPLICATION_JSON_VALUE)
public class ProductController {

    ProductService productService;

    @GetMapping("/{id}")
    public ResponseEntity<Product> getById(@PathVariable Long id) {
        Product product = productService.getById(id);
        return new ResponseEntity<>(product, HttpStatus.OK);
    }

    @PostMapping
    public ResponseEntity<Product> create(@RequestBody Product product) {
        Product newProduct = productService.create(product);
        return new ResponseEntity<>(newProduct, HttpStatus.OK);
    }

    @PutMapping
    public ResponseEntity<Product> update(@RequestBody Product product) {
        Product updatedProduct = productService.update(product);
        return new ResponseEntity<>(updatedProduct, HttpStatus.OK);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity deleteById(@PathVariable Long id) {
        productService.deleteById(id);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @PostMapping("/increase/total/{id}/{num}")
    public ResponseEntity increaseTotal(@PathVariable Long id, @PathVariable Long num) {
        productService.increaseTotal(id, num);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @PostMapping("/reduce/total/{id}/{num}")
    public ResponseEntity reduceTotal(@PathVariable Long id, @PathVariable Long num) {
        productService.reduceTotal(id, num);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @PostMapping("/increase/reserved/{id}/{num}")
    public ResponseEntity increaseReserved(@PathVariable Long id, @PathVariable Long num) {
        productService.increaseReserved(id, num);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @PostMapping("/reduce/reserved/{id}/{num}")
    public ResponseEntity reduceReserved(@PathVariable Long id, @PathVariable Long num) {
        productService.reduceReserved(id, num);
        return new ResponseEntity<>(HttpStatus.OK);
    }
}
