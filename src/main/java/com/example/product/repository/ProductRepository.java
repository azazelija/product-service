package com.example.product.repository;

import com.example.product.entities.ProductEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.lang.NonNull;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository
public interface ProductRepository extends JpaRepository<ProductEntity, Long> {

    @Transactional
    @Modifying
    @Query("update ProductEntity p set p.total = ?2 where p.id = ?1")
    int updateTotalById(@NonNull Long id, @NonNull Long total);

    @Transactional
    @Modifying
    @Query("update ProductEntity p set p.reserved = ?2 where p.id = ?1")
    int updateReservedById(@NonNull Long id, @NonNull Long reserved);
}
