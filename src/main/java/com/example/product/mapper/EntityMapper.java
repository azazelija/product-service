package com.example.product.mapper;

import org.mapstruct.InheritInverseConfiguration;

public interface EntityMapper<E, M> {

    M entityToModel(E entity);

    @InheritInverseConfiguration
    E modelToEntity(M model);
}
