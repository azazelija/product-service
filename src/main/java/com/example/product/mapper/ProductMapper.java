package com.example.product.mapper;

import com.example.product.entities.ProductEntity;
import com.example.product.model.Product;
import lombok.NoArgsConstructor;
import org.mapstruct.InjectionStrategy;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring", injectionStrategy = InjectionStrategy.CONSTRUCTOR, uses = {TypeMapper.class})
public interface ProductMapper extends EntityMapper<ProductEntity, Product> {
}
