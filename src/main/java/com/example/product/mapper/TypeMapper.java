package com.example.product.mapper;

import com.example.product.entities.TypeEntity;
import com.example.product.model.Type;
import org.mapstruct.InjectionStrategy;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring", injectionStrategy = InjectionStrategy.CONSTRUCTOR)
public interface TypeMapper extends EntityMapper<TypeEntity, Type>{
}
