-- liquibase formatted sql
/*
 Описание:                Тип товара
 Первичные ключи:         inline
 Уникальные ключи:        pk_type_id
*/
-- changeset ${user.name}:create_types_tab.sql logicalFilePath:db/changelog/tab/types.sql runOnChange:true splitStatements:true
create table if not exists types
(
    id   bigserial
        constraint pk_type_id primary key,
    name varchar(200) not null
    );


-- changeset ${user.name}:comment_on_columns_types.sql logicalFilePath:db/changelog/tab/types.sql runOnChange:true splitStatements:true
comment on column types.name is 'Тип товара';