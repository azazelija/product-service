-- liquibase formatted sql
/*
 Описание:                Товары
 Первичные ключи:         inline
 Уникальные ключи:        pk_product_id
*/
-- changeset ${user.name}:create_products_tab.sql logicalFilePath:db/changelog/tab/products.sql runOnChange:true splitStatements:true
create table if not exists products
(
    id   bigserial
        constraint pk_product_id primary key,
    type_id     bigint      not null,
    name varchar(200) not null,
    description  varchar(1000),
    price decimal(12,2) not null,
    total bigint not null,
    reserved bigint not null
    );


-- changeset ${user.name}:comment_on_columns_products.sql logicalFilePath:db/changelog/tab/products.sql runOnChange:true splitStatements:true
comment on column products.name is 'Наименование товара';
comment on column products.type_id is 'Тип товара';
comment on column products.description is 'Описание товара';
comment on column products.price is 'Цена товара';
comment on column products.total is 'Количество товара';
comment on column products.reserved is 'Количество зарезервированного товара';