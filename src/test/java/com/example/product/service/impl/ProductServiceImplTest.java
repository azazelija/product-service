package com.example.product.service.impl;

import com.example.product.entities.ProductEntity;
import com.example.product.mapper.ProductMapper;
import com.example.product.mapper.ProductMapperImpl;
import com.example.product.model.Product;
import com.example.product.model.Type;
import com.example.product.repository.ProductRepository;
import com.example.product.service.ProductService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.BeanUtils;

import java.math.BigDecimal;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class ProductServiceImplTest {

    @Mock
    ProductRepository productRepository;

    @Mock
    ProductMapper productMapper;

    @InjectMocks
    ProductServiceImpl productService;

    Product product = new Product();
    ProductEntity productEntity = new ProductEntity();

    @BeforeEach
    void setUp() {
        Long id = 1L;
        Type type = new Type(1L, "Смартфон");
        String name = "Iphone 6s";
        String description = "Rose Gold";
        BigDecimal price = BigDecimal.valueOf(59999.99);
        Long total = 100L;
        Long reserved = 10L;

        product.setId(id);
        product.setType(type);
        product.setName(name);
        product.setDescription(description);
        product.setPrice(price);
        product.setTotal(total);
        product.setReserved(reserved);

        BeanUtils.copyProperties(product, productEntity);
    }

    @Test
    void getById() {
        when(productMapper.entityToModel(any())).thenReturn(product);
        when(productRepository.findById(anyLong())).thenReturn(Optional.ofNullable(productEntity));

        Product returnedProduct = productService.getById(anyLong());
        assertThat(returnedProduct)
                .usingRecursiveComparison()
                .isEqualTo(product);
    }

    @Test
    void create() {
        when(productMapper.modelToEntity(any())).thenReturn(productEntity);
        when(productRepository.save(any())).thenReturn(productEntity);

        product.setId(null);
        Product returnedProduct = productService.create(product);
        assertThat(returnedProduct)
                .usingRecursiveComparison()
                .ignoringFields("id")
                .isEqualTo(product);

        assertThat(returnedProduct.getId())
                .isEqualTo(productEntity.getId());
    }

    @Test
    void update() {
        when(productMapper.modelToEntity(any())).thenReturn(productEntity);
        when(productRepository.findById(anyLong())).thenReturn(Optional.ofNullable(productEntity));
        when(productRepository.save(any())).thenReturn(productEntity);

        Product returnedProduct = productService.update(product);
        assertThat(returnedProduct)
                .usingRecursiveComparison()
                .isEqualTo(product);
    }

    @Test
    void deleteById() {
        productService.deleteById(anyLong());
    }

    @Test
    void increaseTotal() {
        when(productRepository.findById(anyLong())).thenReturn(Optional.ofNullable(productEntity));

        productService.increaseTotal(productEntity.getId(), 50L);

        Long total = productEntity.getTotal() + 50L;
        verify(productRepository).updateTotalById(productEntity.getId(), total);
    }

    @Test
    void reduceTotal() {
        when(productRepository.findById(anyLong())).thenReturn(Optional.ofNullable(productEntity));

        productService.reduceTotal(productEntity.getId(), 50L);

        Long total = productEntity.getTotal() - 50L;
        verify(productRepository).updateTotalById(productEntity.getId(), total);
    }

    @Test
    void increaseReserved() {
        when(productRepository.findById(anyLong())).thenReturn(Optional.ofNullable(productEntity));

        productService.increaseReserved(productEntity.getId(), 50L);

        Long reserved = productEntity.getReserved() + 50L;
        verify(productRepository).updateReservedById(productEntity.getId(), reserved);
    }

    @Test
    void reduceReserved() {
        when(productRepository.findById(anyLong())).thenReturn(Optional.ofNullable(productEntity));

        productService.reduceReserved(productEntity.getId(), 50L);

        Long reserved = productEntity.getReserved() - 50L;
        verify(productRepository).updateReservedById(productEntity.getId(), reserved);
    }
}